const max = 60;
let counter = max;
let continueBeeping;
g.setFont("6x8", 6);
g.setFontAlign(0, 0);
	
function ringing() {
	startTimer = undefined;
	g.flip();
	Bangle.buzz();
	Bangle.beep(200, 4000);
	E.showMessage("Time's up");
}

function resetCounter() {
	counter = max;
	clearInterval(continueBeeping);
	g.setFont("6x8", 6);
	g.setFontAlign(0, 0);
	startTimer = setInterval(countDown, 1000);
	return;
}

function outOfTime() {
	setWatch(resetCounter, BTN2);
	clearInterval(startTimer);
	continueBeeping = setInterval(ringing, 5000);
}


function countDown() {
	if (counter <= 0) outOfTime();
	g.clear();
	g.drawString(counter, 120, 120);
	console.log(counter);
	counter--;
}

let startTimer = setInterval(countDown, 1000);