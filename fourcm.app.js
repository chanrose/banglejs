Bangle.setLCDPower(1);
Bangle.setLCDTimeout(0);
g.clear();

let total = 0; 
g.setFont("6x8", 2);
g.setFontAlign(0, 0);
g.drawString("Ruler in cm", 120, 50);
// Horizontal line
g.drawLine(0, 120, 240, 120);
// The first cm line
g.drawLine(15, 100, 15, 140);
// Second vertical cm
g.drawLine(115, 100, 115, 140);
// Third vertical line
g.drawLine(215, 100, 215, 140);
g.setFont("6x8", 1);
g.setFontAlign(0, 0);

function showRuler() {
	g.clearRect(0, 140, 230, 175);
	
	// The first cm line
	g.drawString(total+1, 15, 155);
	
	// Second vertical cm
	g.drawString(total+2, 115, 155);
	
	// Third vertical line
	g.drawString(total+3, 215, 155);
}

function totalPlus() {
	total += 4;
	showRuler();
}

function totalMinus() {
	total -= 4;
	showRuler();
}
function totalReset() {
	total = 0;
	showRuler();
}



showRuler();
setWatch(totalPlus, BTN1, {repeat:true});
setWatch(totalMinus, BTN3, {repeat:true});
setWatch(totalReset, BTN2, {repeat:true});



