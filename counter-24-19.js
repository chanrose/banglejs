let counter = 0;
let start = new Date();
let allowCounter = true;


function addCounter() {
	if (!allowCounter) {
		g.clear();
		E.showMessage("Please reset \nthe counter first");
		return;
	}
	counter++;
	g.clear();
	draw();
}

function endCount() {
	if (!allowCounter) {
		g.clear();
		E.showMessage("Please reset \nthe counter first");
		return;
	}
	allowCounter = false;
	const end = new Date();
	const diff = (end - start) / 1000;
	if (diff > 60) {
		E.showMessage(`Count: ${counter}, \nSpent: ${(diff/60).toFixed(0)}:${(diff%60).toFixed(0)}`);
		return;							
	}
	const sec = `${diff.toFixed(0)}`.padStart(2, "0");
	E.showMessage(`Count: ${counter},\nSpent: ${sec} sec\n${start.getHours()}:${start.getMinutes()} - ${end.getHours()}:${end.getMinutes()}`);
}

function resetCounter() {
	E.showMessage("Timer reset!");
	setTimeout(() => {
		allowCounter = true;
		g.clear();
		start = new Date();
		counter = 0;
		draw();
	}, 1000);
}

function draw() {
	g.clear();
	g.setFont("6x8", 5);
	g.setFontAlign(0, 0);
	g.drawString(counter, 120, 120);
	g.setFont("6x8", 2);
	const now = new Date();
	const sm = `${start.getMinutes()}`.padStart(2, "0"), ss = `${start.getSeconds()}`.padStart(2, "0");
	g.drawString(`${start.getHours()}:${sm}:${ss}`, 120, 80);
	const m = `${now.getMinutes()}`.padStart(2, "0"), s = `${now.getSeconds()}`.padStart(2, "0");
	g.drawString(`${now.getHours()}:${m}:${s}`, 120, 160);
}

Bangle.on('lcdPower',on=>{
  if (on && allowCounter) {
    draw(); // draw immediately
  }
});


g.clear();
draw();
setWatch(addCounter, BTN1, {repeat:true});
setWatch(resetCounter, BTN2, {repeat:true});
setWatch(endCount, BTN3, {repeat:true});

