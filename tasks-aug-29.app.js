var f = require("Storage").read("tasks.csv");
const tasks = f.split("\n").map((item) => 
	item.split(",")
);

const w = g.getWidth(), h = g.getHeight();
let selected = 0, menuScroll = 0, menuShowing = false;
let lastY = 0;

function drawTasks() {
	console.log(selected);
	const m = w/2, n = 8;
	g.reset().setFont("6x8", 1).setFontAlign(0, 0);
	if (selected >= n+menuScroll) menuScroll = 1 + selected - n;
	if (selected < menuScroll) menuScroll = selected;
	for (let i = 0; i < n; i++) {
		const task = tasks[i + menuScroll];
		if (!task) break;
		const y = 10 + i * 20;
		if (i+menuScroll==selected) {
			g.setFont("6x8", 2);
			g.drawString(task[0], 120, y + 32); 
			lastY = y + 32;
			if (tasks[selected][1] === "T") {
				g.drawLine(10, lastY, 230, lastY);	
			}
		} else {
			g.setFont("6x8", 1);	
			if (task[1] === "T") {
				g.drawString("[X] " + task[0], 120, y + 32);
			} else 
			g.drawString("[ ] " + task[0], 120, y + 32);
		}
	}
	
}

g.clear();
drawTasks();

Bangle.setUI("updown", dir => {
	g.clear();
	if (dir) {
		selected += dir;	
		if (selected < 0) selected = tasks.length-1;
		if (selected >= tasks.length) selected = 0;	
	} else {
		if (tasks[selected][1] === "F") {
			tasks[selected][1] = "T";	
			g.drawLine(10, lastY, 230, lastY);
		} else tasks[selected][1] = "F";
		require("Storage").write("tasks.csv", tasks.join("\n"));
	}
	drawTasks();
});
