function openLauncher() {
	g.clear();
	Bangle.showLauncher();
}

function draw() {
	const now = new Date();
	const h = now.getHours(), m = now.getMinutes(), mon = now.getMonth(), day = now.getDate();
	const time = h + ":" + `${m}`.padStart(2, '0');
	const dateText = mon + "<-" + `${day}`.padStart(2, '0');
	g.reset();
	g.clearRect(50, 50, 200, 200);
	g.setFont("6x8", 6);
	g.drawString(time, 50, 90, true);
	g.setFont("6x8", 2);
	g.setFontAlign(0, 1);
	g.drawString(dateText, 120, 160, true);
}

g.clear();
draw();
let secondInterval = setInterval(draw, 1000);

// Run if lcd is on
Bangle.on('lcdPower', on => {
	if (secondInterval) clearInterval(secondInterval);
	secondInterval = undefined;
	if (on) {
		secondInterval = setInterval(draw, 1000);
		draw();
	}
});

// Load the widget	
Bangle.loadWidgets();
Bangle.drawWidgets();

// Show launcher when the middle btn pressed
setWatch(openLauncher, BTN2, { repeat: false, edge: "falling" });