# BangleJS

This repository is programs for BangleJS that is made or customized from Bangle.js Apps library

## Tasks

This is to do app concept in which can be mark or unmark as done. This app will read and write another file name "tasks.csv". Current goal of this app is to be able to add / remove tasks on laptop when connected with bluetooth. 

### Current Flaw

- Only show 8 lists
- Text larger than 18 characters when selected will not be readable
- Hasn't work on the add/remove tasks on the laptop yet
- It reads the file first even before checking whether the file is exist or not

### Current Implementation

- Read file
- Show a list of tasks
- Mark / Unmark as done
- Update the file when mark/unmark

## Counter

Simple counter app that can record the start time, the duration, and end time, and units of count.

## Battery Widget

Show battery when it is not charging and show .:. when it is charging, updates about 5 or 10 sec...

## Clock Face

Show the H:MM on the center and below it is a date (DD). Only update when the LCD is on at first or BTN1 is pressed.

## 4 CM

Suggested by Avanier, made a ruler app for measuring small object, BTN1 to increase the measurement, BTN3 for decrease the measurement, and BTN2 for reseting. The measurement starts expect from the edge of the letter W to the other side edge of the E.

## 60 sec

Simple count down app that you can find on the BangleJS App Loader. Learnt from tutorial.


## Launcher

Just modified to remove the icons and the color. Might be sorting as well.
