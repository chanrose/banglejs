let counter = 0;
let start = new Date();
let allowCounter = true;


function addCounter() {
	if (!allowCounter) {
		g.clear();
		E.showMessage("Please reset \nthe counter first");
		return;
	}
	counter++;
	g.clear();
	draw();
}

function endCount() {
	if (!allowCounter) {
		g.clear();
		E.showMessage("Please reset \nthe counter first");
		return;
	}
	allowCounter = false;
	const end = new Date();
	const diff = (end - start) / 1000;
	if (diff > 60) {
		E.showMessage(`Count: ${counter}, \nSpent: ${(diff/60).toFixed(0)}:${((diff).toFixed(2)%60)}`);
		return;							
	}
	E.showMessage(`Count: ${counter},\nSpent: ${diff.toFixed(0)} sec`);
}

function resetCounter() {
	allowCounter = true;
	g.clear();
	start = new Date();
	counter = 0;
	draw();
}

function draw() {
	g.clearRect(80, 80, 160, 160);
	g.setFont("6x8", 5);
	g.setFontAlign(0, 0);
	g.drawString(counter, 120, 120);
}


g.clear();
draw();
setWatch(addCounter, BTN1, {repeat:true});
setWatch(resetCounter, BTN2, {repeat:true});
setWatch(endCount, BTN3, {repeat:true});


