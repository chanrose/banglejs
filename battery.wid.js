WIDGETS = {};
(() => {
	
	function draw() {
		g.reset();
		g.setFont("6x8", 2);
		if (!Bangle.isCharging()) {
			g.drawString(`${E.getBattery()}`, this.x, this.y);
		} else {
			g.drawString(".:.", this.x, this.y);
		}
	}
	
	setInterval(function() {
		WIDGETS["battery"].draw(WIDGETS["battery"]);
	}, 10000);
	
	
	WIDGETS["battery"]={
		area: "br",
		width: 28,
		draw:draw
	};
	
})();
