function draw() {
	const now = new Date();
	const h = now.getHours(), m = now.getMinutes(), day = now.getDate();
	const time = h + ":" + `${m}`.padStart(2, '0');
	
	g.clear();
	g.setFont("6x8", 6);
	g.setFontAlign(0,0);
	g.drawString(time, 120, 110, true);
	g.setFont("6x8", 2);
	g.drawString(day, 120, 160, true);
}


g.reset();
draw();
// Load the widget	
Bangle.loadWidgets();
Bangle.drawWidgets();

// Update when turn on
Bangle.on('lcdPower', on=> {
	if (on) {
		draw();	
	}
});

// Update when pressed
setWatch(draw, BTN1, {repeat: true});

// Show launcher when the middle btn pressed
setWatch(Bangle.showLauncher, BTN2, { repeat: false, edge: "falling" });